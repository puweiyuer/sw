<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/11
 * Time: 13:45
 */

namespace Sw\Http\Server\Web;


class Request
{
    public $action;

    public $controllers;
    /**
     * 路由
     */
    public function router(array $request=[])
    {
        if(!empty($request['request_uri'])){
            $mapRoute=trim($request['request_uri'],'/');
            $mapRoute=explode('/',$mapRoute);
        }
        $this->controllers=!empty($mapRoute[0])?ucwords($mapRoute[0]):'Index';
        $this->action=!empty($mapRoute[1])?$mapRoute[1]:'index';
    }

    /**
     * 实例化控制器
     * @return mixed
     */
    public function controllers()
    {
        //>>.控制器
        $class  = "App\Controllers\\".$this->controllers;
        //>>.方法
        $action=$this->action;
        $file=APP."/app/Controllers/".$this->controllers.".php";
        if (is_file($file)){
            $c=new $class();
            try{
                return $c->$action();

            }catch (\Throwable $e){
                return '方法不存在'.$action;
            }
        }else{
            return '控制器没有找到'.$this->controllers;
        }

    }


}