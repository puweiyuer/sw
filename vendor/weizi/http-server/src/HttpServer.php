<?php
namespace Sw\Http\Server\Web;

use Swoole\Http\Request;
use Swoole\Http\Response;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/11
 * Time: 11:00
 */
class HttpServer
{

    protected $server;

    /**
     * 初始化http服务器
     * HttpServer constructor.
     * @param $server
     */
    public function __construct($server)
    {
         $this->server= new \swoole_http_server($server['http']['host'],$server['http']['port']) ;
        var_dump("ip:{$server['http']['host']} port:{$server['http']['port']}");
    }

    /**
     * 添加配置
     * @param $setting
     */
    public function setSetting($setting){
        if(!empty($setting))
            $this->server->set($setting);
    }

    /**
     * 添加事件
     */
    public function on()
    {
        $this->server->on('request',[$this,'onRequest']);
    }

    /**
     * onRequest event callback
     * Each request will create an coroutine
     *
     * @param Request $request
     * @param Response $response
     * @throws \InvalidArgumentException
     */
    public function onRequest(Request $request, Response $response)
    {
        if(isset($request->server['request_uri'])&&$request->server['request_uri']=='/favicon.ico'){
            $response->end("");
        }else{

            //>>.简单路由解析
            $sw_request=new \Sw\Http\Server\Web\Request();
            $sw_request->router($request->server);
            $data=$sw_request->controllers();
            $response->header("Content-Type", "text/html; charset=utf-8");
            $response->end($data);
        }
    }

    public function start(){
        $this->server->start();
    }

}

