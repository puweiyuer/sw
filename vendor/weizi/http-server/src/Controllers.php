<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/11
 * Time: 15:02
 */

namespace Sw\Http\Server\Web;


class Controllers
{

    protected $view_file=APP."/View/";
    public function renderPartial($view, $params = [])
    {

        foreach($params as $key=>$value){
            $$key=$value;
        }

        $file=$this->view_file.$view.'.php';
        if(!is_file($file))
            return '视图不存在'.$view;
        //>>.swoole里面要使用这个才可以访问视图
        ob_start();
        include  $file;
        //>>.关闭
        $output = ob_get_clean();
        return $output;
    }
}