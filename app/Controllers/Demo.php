<?php
namespace App\Controllers;

use Sw\Http\Server\Web\Controllers;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/11
 * Time: 14:01
 */

class Demo extends Controllers
{

    public function index()
    {
        $data=['status'=>1,'info'=>'成功'];
        return $this->renderPartial('Demo/index',compact('data'));
    }
}