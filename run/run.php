#!/usr/bin/env php
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/11
 * Time: 9:21
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';
$server=require_once dirname(__DIR__)."/config/server.php";
! defined('APP') && define('APP', dirname(__DIR__, 1));
$application=new \Sw\Http\Server\Web\Application($server);
$application->run();
