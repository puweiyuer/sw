<?php
! defined('DS') && define('DS', DIRECTORY_SEPARATOR);
// App name
! defined('APP_NAME') && define('APP_NAME', 'sw');
// Project base path
! defined('BASE_PATH') && define('BASE_PATH', dirname(__DIR__, 1));

return [
    'server'  => [
        'pfile'      => '/tmp/swoft.pid',
        'pname'      =>  'php-swoft',
        'tcpable'    =>  true,
        'cronable'   => false,
        'autoReload' =>  true,
    ],
    'http'    => [
        'host'  => '0.0.0.0',
        'port'  =>  8099,
        'mode' => SWOOLE_PROCESS,
        'type'  =>  SWOOLE_SOCK_TCP,
    ],
];